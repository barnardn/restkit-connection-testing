//
//  AppController.h
//  RestKitTest
//
//  Created by Norm Barnard on 2/27/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RKManagedObjectStore;

@interface AppController : NSObject

+ (AppController *)appController;


- (NSURL *)urlForResourceInApplicationSupport:(NSString *)resourceName;
- (NSManagedObjectModel *)dataModel;
- (RKManagedObjectStore *)objectStore;
- (void)importData;

@end
