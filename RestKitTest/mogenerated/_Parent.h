// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Parent.h instead.

#import <CoreData/CoreData.h>


extern const struct ParentAttributes {
	__unsafe_unretained NSString *childIDS;
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *name;
} ParentAttributes;

extern const struct ParentRelationships {
	__unsafe_unretained NSString *children;
} ParentRelationships;

extern const struct ParentFetchedProperties {
} ParentFetchedProperties;

@class Child;

@class NSObject;



@interface ParentID : NSManagedObjectID {}
@end

@interface _Parent : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ParentID*)objectID;





@property (nonatomic, strong) id childIDS;



//- (BOOL)validateChildIDS:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* identity;



//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *children;

- (NSMutableSet*)childrenSet;





@end

@interface _Parent (CoreDataGeneratedAccessors)

- (void)addChildren:(NSSet*)value_;
- (void)removeChildren:(NSSet*)value_;
- (void)addChildrenObject:(Child*)value_;
- (void)removeChildrenObject:(Child*)value_;

@end

@interface _Parent (CoreDataGeneratedPrimitiveAccessors)


- (id)primitiveChildIDS;
- (void)setPrimitiveChildIDS:(id)value;




- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveChildren;
- (void)setPrimitiveChildren:(NSMutableSet*)value;


@end
