// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Child.h instead.

#import <CoreData/CoreData.h>


extern const struct ChildAttributes {
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *name;
} ChildAttributes;

extern const struct ChildRelationships {
	__unsafe_unretained NSString *parent;
} ChildRelationships;

extern const struct ChildFetchedProperties {
} ChildFetchedProperties;

@class Parent;




@interface ChildID : NSManagedObjectID {}
@end

@interface _Child : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ChildID*)objectID;





@property (nonatomic, strong) NSString* identity;



//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Parent *parent;

//- (BOOL)validateParent:(id*)value_ error:(NSError**)error_;





@end

@interface _Child (CoreDataGeneratedAccessors)

@end

@interface _Child (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (Parent*)primitiveParent;
- (void)setPrimitiveParent:(Parent*)value;


@end
