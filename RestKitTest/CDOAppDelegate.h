//
//  CDOAppDelegate.h
//  RestKitTest
//
//  Created by Norm Barnard on 2/27/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
