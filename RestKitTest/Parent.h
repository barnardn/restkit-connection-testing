#import "_Parent.h"

@class RKEntityMapping;

@interface Parent : _Parent {}

+ (RKEntityMapping *)entityMapping;

@end
