#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import "AppController.h"
#import "Child.h"
#import "Parent.h"


@interface Parent ()

// Private interface goes here.

@end


@implementation Parent


+ (RKEntityMapping *)entityMapping;
{
    RKEntityMapping *pMap = [RKEntityMapping mappingForEntityForName:[Parent entityName] inManagedObjectStore:[[AppController appController] objectStore]];
    [pMap setIdentificationAttributes:@[ParentAttributes.identity]];
    [pMap addAttributeMappingsFromArray:@[ ParentAttributes.identity, ParentAttributes.name, ParentAttributes.childIDS]];
    [pMap addConnectionForRelationship:@"children" connectedBy:@{ @"childIDS" : @"identity"}];
    return pMap;
}



@end
