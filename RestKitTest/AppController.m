//
//  AppController.m
//  RestKitTest
//
//  Created by Norm Barnard on 2/27/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>

#import "AppController.h"
#import "Child.h"
#import "Parent.h"

@implementation AppController

+ (AppController *)appController
{
    static AppController *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (!self) return nil;
    NSURL *appSupport = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    BOOL isDir;
    BOOL doesExist = [[NSFileManager defaultManager] fileExistsAtPath:[appSupport path] isDirectory:&isDir];
    if (!doesExist) {
        NSError *error;
        BOOL ok = [[NSFileManager defaultManager] createDirectoryAtURL:appSupport withIntermediateDirectories:YES attributes:nil error:&error];
        ZAssert(ok, @"Whoa! Unable to create app support directory: %@", error);
    }
    [self nukeStore];
    return self;
}

- (void)nukeStore
{
    NSURL *storeURL = [self urlForResourceInApplicationSupport:@"Database.sqlite"];
    [[NSFileManager defaultManager] removeItemAtPath:[storeURL path] error:nil];
}

- (NSURL *)urlForResourceInApplicationSupport:(NSString *)resourceName
{
    NSURL *appSupport = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *resourceURL = [NSURL fileURLWithPathComponents:@[ [appSupport path], resourceName]];
    return resourceURL;
}

- (NSManagedObjectModel *)dataModel
{
    static NSManagedObjectModel *staticModel;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
        staticModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    });
    return staticModel;
}

- (RKManagedObjectStore *)objectStore
{
    static RKManagedObjectStore *staticStore;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        RKLogConfigureByName("RestKit/Network", RKLogLevelDebug);
        RKLogConfigureByName("RestKit/CoreData", RKLogLevelDebug);
        staticStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:[self dataModel]];
        [staticStore createPersistentStoreCoordinator];
        NSString *libStorePath = [[self urlForResourceInApplicationSupport:@"Database.sqlite"] path];
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        NSError *error = nil;
        NSPersistentStore *store = [staticStore addSQLitePersistentStoreAtPath:libStorePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:options error:&error];
        ZAssert(store != nil, @"Whoa! Can't create persistent store,  %@ %@", libStorePath, error);
        [staticStore createManagedObjectContexts];
        RKInMemoryManagedObjectCache *cache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:[staticStore persistentStoreManagedObjectContext]];
        [staticStore setManagedObjectCache:cache];
    });
    return staticStore;
}

- (void)importData
{
    NSPersistentStore *store = [[[[self objectStore] persistentStoreCoordinator] persistentStores] lastObject];
    RKManagedObjectImporter *importer = [[RKManagedObjectImporter alloc] initWithPersistentStore:store];
    
    NSString *importPath = [[NSBundle mainBundle] pathForResource:@"datamodel" ofType:@"json"];
    NSError *error;
    NSInteger importCount = 0;
    importCount = [importer importObjectsFromItemAtPath:importPath withMapping:[Child entityMapping] keyPath:@"children" error:&error];
    ZAssert(importCount > 0, @"Meh, no kids imported %@", error);
    importCount = [importer importObjectsFromItemAtPath:importPath withMapping:[Parent entityMapping] keyPath:@"parents" error:&error];
    ZAssert(importCount > 0, @"Meh, no parents imported %@", error);
    ZAssert([importer finishImporting:&error], @"Import failed: %@", error);
}



@end
