#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>

#import "AppController.h"
#import "Child.h"


@interface Child ()

// Private interface goes here.

@end


@implementation Child

+ (RKEntityMapping *)entityMapping;
{
    RKEntityMapping *map = [RKEntityMapping mappingForEntityForName:[Child entityName] inManagedObjectStore:[[AppController appController] objectStore]];
    [map setIdentificationAttributes:@[ChildAttributes.identity]];
    [map addAttributeMappingsFromArray:@[ ChildAttributes.identity, ChildAttributes.name]];
    return map;
}


@end
