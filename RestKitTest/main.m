//
//  main.m
//  RestKitTest
//
//  Created by Norm Barnard on 2/27/13.
//  Copyright (c) 2013 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDOAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDOAppDelegate class]));
    }
}
